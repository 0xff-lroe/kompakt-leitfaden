[[leitfaden-knoten-einleitung]]
== Einleitung

In den folgenden Abschnitten geht es darum, wie man nun zu seinem eigenen "Knoten" kommt.
Anhand eines fiktiven aufzubauenden Knotens werden alle notwendigen Schritte dokumentiert, die unternommen werden sollten, um die Hardware zu montieren und die Software so zu konfigurieren, dass eine Teilnahme am FunkFeuer-Netz möglich ist.

Wie ein solcher "Knoten" aussieht und was man sich darunter konkret vorstellen kann wird in Abschnitt <<leitfaden-grundlagen-knoten>> beschrieben.

[NOTE]
====
Alle folgenden Anweisungen, Konfigurationen und beschriebenen Schritte sind als *Empfehlung* zu verstehen!
Die Knoten-Konfiguration hängt nämlich sehr stark von den örtlichen Gegebenheiten oder etwa den Anforderungen an den jeweiligen Knoten ab:

* Ist die Befestigung an der Hauswand, auf einem Mast am Dach oder am Rauchfang geplant?
* Findet der Router im Gebäude Platz oder muss er Outdoor befestigt werden?
* Ist nur eine Antenne oder sind mehrere geplant?
* Gibt es bereits Nachbar-Knoten, zu denen eine Verbindung hergestellt wird, oder wird vorerst ein Insel-Knoten aufgebaut?
* ...

Man sieht, dass es sehr viele Faktoren gibt, die die konkrete Ausstattung und Ausgestaltung des jeweiligen Knotens beeinflussen.
====

[NOTE]
====
Das hier beschriebene Setup wurde nach folgenden Gesichtspunkten ausgewählt:

* ausgewogenes Verhältnis von Kosten und Leistung
* einfache Einrichtung (größtenteils per Weboberfläche)
* Verwendung von nur je einer IP-Adresse (IPv4 und IPv6) pro Knoten
* Router:
** Gigabit (1000 Mbps) Geschwindigkeit zwischen lokalem Netzwerk und Router (bei AC fähiger Antenne bis zur Antenne)
** PoE-Fähigkeit
* Antenne(n):
** per PoE mit Strom versorgt, daher aus der Ferne per Weboberfläche aus-/einschaltbar
** bestmögliche Bandbreite

Weitere Informationen dazu findet man auch im Dokument <<bib-er-setup>>.
====