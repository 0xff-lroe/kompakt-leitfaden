require 'asciidoctor/extensions'

include Asciidoctor

# An extension that introduces custom admonition types, complete
# with a custom icons.
#
# Usage
#
#   [QUESTION]
#   ====
#   What's the main tool for selecting colors?
#   ====
#
# or
#
#   [QUESTION]
#   What's the main tool for selecting colors?
#
# Available types are:
# - CHECKLIST
# - EXAMPLE
# - EXPERIMENT
# - FOLLOWUP
# - HANDSON
# - QUESTION
# - TERMINAL
# - WIP
#
class ChecklistAdmonitionBlock < Extensions::BlockProcessor
  use_dsl
  named :CHECKLIST
  on_contexts :example, :paragraph

  def process parent, reader, attrs
    attrs['name'] = 'checklist'
    attrs['caption'] = 'Checklist'
    create_block parent, :admonition, reader.lines, attrs, content_model: :compound
  end
end

class ExampleAdmonitionBlock < Extensions::BlockProcessor
  use_dsl
  named :EXAMPLE
  on_contexts :example, :paragraph

  def process parent, reader, attrs
    attrs['name'] = 'example'
    attrs['caption'] = 'Example'
    create_block parent, :admonition, reader.lines, attrs, content_model: :compound
  end
end

class ExperimentAdmonitionBlock < Extensions::BlockProcessor
  use_dsl
  named :EXPERIMENT
  on_contexts :example, :paragraph

  def process parent, reader, attrs
    attrs['name'] = 'experiment'
    attrs['caption'] = 'Experiment'
    create_block parent, :admonition, reader.lines, attrs, content_model: :compound
  end
end

class FollowUpAdmonitionBlock < Extensions::BlockProcessor
  use_dsl
  named :FOLLOWUP
  on_contexts :example, :paragraph

  def process parent, reader, attrs
    attrs['name'] = 'followup'
    attrs['caption'] = 'Follow-Up'
    create_block parent, :admonition, reader.lines, attrs, content_model: :compound
  end
end

class HandsOnAdmonitionBlock < Extensions::BlockProcessor
  use_dsl
  named :HANDSON
  on_contexts :example, :paragraph

  def process parent, reader, attrs
    attrs['name'] = 'handson'
    attrs['caption'] = 'Hands-On'
    create_block parent, :admonition, reader.lines, attrs, content_model: :compound
  end
end

class QuestionAdmonitionBlock < Extensions::BlockProcessor
  use_dsl
  named :QUESTION
  on_contexts :example, :paragraph

  def process parent, reader, attrs
    attrs['name'] = 'question'
    attrs['caption'] = 'Question'
    create_block parent, :admonition, reader.lines, attrs, content_model: :compound
  end
end

class TerminalAdmonitionBlock < Extensions::BlockProcessor
  use_dsl
  named :TERMINAL
  on_contexts :example, :paragraph

  def process parent, reader, attrs
    attrs['name'] = 'terminal'
    attrs['caption'] = 'Terminal'
    create_block parent, :admonition, reader.lines, attrs, content_model: :compound
  end
end

class WipAdmonitionBlock < Extensions::BlockProcessor
  use_dsl
  named :WIP
  on_contexts :example, :paragraph

  def process parent, reader, attrs
    attrs['name'] = 'wip'
    attrs['caption'] = 'Work-In-Progress'
    create_block parent, :admonition, reader.lines, attrs, content_model: :compound
  end
end

class AdmonitionsBlockDocinfo < Extensions::DocinfoProcessor
  use_dsl

  def process doc
    if (doc.basebackend? 'html') && doc.backend != 'pdf'
      '<style>
.admonitionblock td.icon .icon-checklist:before {content:"\f0ca";color:#556270;}
.admonitionblock td.icon .icon-example:before {content:"\f1c9";color:#556270;}
.admonitionblock td.icon .icon-experiment:before {content:"\f0c3";color:#A1C820;}
.admonitionblock td.icon .icon-followup:before {content:"\f277";color:#556270;}
.admonitionblock td.icon .icon-handson:before {content:"\f044";color:#556270;}
.admonitionblock td.icon .icon-question:before {content:"\f128";color:#871452;}
.admonitionblock td.icon .icon-terminal:before {content:"\f120";color:#556270;}
.admonitionblock td.icon .icon-wip:before {content:"\f0ad";color:#BF6900;}
</style>'
    end
  end
end
