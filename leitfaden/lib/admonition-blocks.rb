RUBY_ENGINE == 'opal' ? (require 'admonition-blocks/extension') : (require_relative 'admonition-blocks/extension')

Extensions.register do
  block ChecklistAdmonitionBlock
  block ExampleAdmonitionBlock
  block ExperimentAdmonitionBlock
  block FollowUpAdmonitionBlock
  block HandsOnAdmonitionBlock
  block QuestionAdmonitionBlock
  block TerminalAdmonitionBlock
  block WipAdmonitionBlock
  docinfo_processor AdmonitionsBlockDocinfo
end
