package at.funkfeuer.dokumentation.extensions.browserframe;

import java.awt.*;
import java.awt.image.ImageObserver;
import java.util.concurrent.CountDownLatch;

class ImageCompleteWaiter implements ImageObserver {

    private final CountDownLatch latch;

    protected ImageCompleteWaiter(final CountDownLatch latch) {
        this.latch = latch;
    }

    @Override
    public boolean imageUpdate(final Image observedImg, final int flags, final int x, final int y, final int width, final int height) {
        if ((flags & ALLBITS) == ALLBITS) {
            latch.countDown();
            return false;
        } else {
            return true;
        }
    }
}
