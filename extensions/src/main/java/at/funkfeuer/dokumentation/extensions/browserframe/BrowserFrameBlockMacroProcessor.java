package at.funkfeuer.dokumentation.extensions.browserframe;

import at.funkfeuer.dokumentation.extensions.Util;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.asciidoctor.ast.Block;
import org.asciidoctor.ast.StructuralNode;
import org.asciidoctor.extension.*;
import org.asciidoctor.log.LogRecord;
import org.asciidoctor.log.Severity;

import java.awt.image.BufferedImage;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

@Name("browserframe")
@Format(FormatType.LONG)
@DefaultAttribute(key = "url", value = "")
public class BrowserFrameBlockMacroProcessor extends BlockMacroProcessor {

    public BrowserFrameBlockMacroProcessor() {
        this(null);
    }

    public BrowserFrameBlockMacroProcessor(final String macroName) {
        this(macroName, new HashMap<>());
    }

    public BrowserFrameBlockMacroProcessor(final String macroName, final Map<String, Object> config) {
        super(macroName, config);
    }

    @Override
    public Object process(final StructuralNode parent, final String target, final Map<String, Object> attributes) {
        // Generate framed image
        try {
            final Path imagesDirPath = Util.getImagesDirPath(this, parent);
            final Path targetPath = imagesDirPath.resolve(target).normalize();

            if (!targetPath.toFile().exists()) {
                log(Severity.ERROR, "Unable to load image '" + targetPath + "'");
                return null;
            }

            final BufferedImage image = BrowserFrameUtil.loadImage(targetPath);
            final BufferedImage browserFrameImage = BrowserFrameUtil.browserFrame(image, BrowserFrame.GENERIC, Util.getStringAttributeValue(attributes, "url"));

            final Path derivedTargetPath = BrowserFrameUtil.getDerivedPath(targetPath, "_browser_frame", "png");
            final Path derivedRelativeTargetPath = imagesDirPath.relativize(derivedTargetPath);

            if (BrowserFrameUtil.saveImagePNG(browserFrameImage, derivedTargetPath)) {
                // Process asciidoc
                final String newTarget = derivedRelativeTargetPath.toString().replace("\\","/");

                final Map<Object, Object> options = new HashMap<>();
                options.put("type", "image");
                options.put("target", newTarget);

                final Map<String, Object> attrs  = new HashMap<>();
                attrs.putAll(attributes);
                attrs.remove("url");
                attrs.remove("title");
                attrs.remove("caption");

                final String title = (String) attributes.get("title");
                final String sTitle;
                if (StringUtils.isNotBlank(title)) {
                    sTitle = title.trim();
                } else {
                    sTitle = "";
                }

                final String caption = (String) attributes.get("title");
                final String sCaption;
                if (attributes.containsKey("caption")) {
                    sCaption = caption.trim();
                } else if (StringUtils.isNotBlank(sTitle)) {
                    sCaption = parent.getDocument().getAttributes().get("figure-caption") + " " + parent.getDocument().getAndIncrementCounter("figure-number") + ". ";
                } else {
                    sCaption = "";
                }

                if (!attributes.containsKey("alt")) {
                    attrs.put("alt", "");
                } else {
                    attrs.put("alt", attributes.get("alt"));
                }
                if (!attrs.containsKey("default-alt")) {
                    attrs.put("default-alt", attrs.get("alt"));
                }
                attrs.put("imagesdir", parent.getDocument().getAttributes().get("imagesdir"));
                attrs.put("target", newTarget);

                final Block block = createBlock(parent, "image", "", attrs, options);
                block.setTitle(sTitle);
                block.setCaption(sCaption);
                return block;
            } else {
                log(Severity.ERROR, "Unable to save image");
            }
        } catch (Exception ex) {
            log(Severity.ERROR, "Unable to merge image with browser frame. Message: " + ExceptionUtils.getStackTrace(ex));
        }
        return null;
    }

    private void log(final Severity severity, final String message) {
        try {
            log(new LogRecord(severity, message));
        } catch (Exception ex) {
            //
        }
    }
}






