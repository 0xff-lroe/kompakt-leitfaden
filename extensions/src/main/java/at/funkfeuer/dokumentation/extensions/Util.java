package at.funkfeuer.dokumentation.extensions;

import org.apache.commons.lang3.StringUtils;
import org.asciidoctor.ast.ContentNode;
import org.asciidoctor.extension.Processor;
import org.asciidoctor.log.LogRecord;
import org.asciidoctor.log.Severity;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

public class Util {

	private Util() {
		//
	}

	public static String getStringAttributeValueOrDefault(final Map<String, Object> attributes, final String key, final String defaultValue) {
		final String result = getStringAttributeValue(attributes, key);
		return (result != null) ? result : defaultValue;
	}

	public static String getStringAttributeValue(final Map<String, Object> attributes, final String key) {
		if (attributes == null || StringUtils.isBlank(key)) {
			return null;
		}

		final Object value = attributes.get(StringUtils.trim(key));
		if (value != null) {
			if (value instanceof String) {
				return (String) value;
			} else {
				return value.toString();
			}
		}

		return null;
	}

	public static boolean getBoolAttributeValueOrDefault(final Map<String, Object> attributes, final String key, final boolean defaultValue) {
		final Boolean result = getBoolAttributeValue(attributes, key);
		return (result != null) ? result : defaultValue;
	}

	public static Boolean getBoolAttributeValue(final Map<String, Object> attributes, final String key) {
		if (attributes == null || StringUtils.isBlank(key)) {
			return null;
		}

		final Object value = attributes.get(StringUtils.trim(key));
		if (value != null) {
			if (value instanceof String) {
				return Boolean.parseBoolean((String) value);
			} else if (value instanceof Boolean) {
				return (Boolean) value;
			} else {
				return Boolean.parseBoolean(value.toString());
			}
		}

		return null;
	}

	public static Path getImagesDirPath(final Processor processor, final ContentNode contentNode) {
		if (contentNode == null) {
			return null;
		}

		//log(processor, Severity.DEBUG, "ATTRS: " + contentNode.getDocument().getAttributes());
		//log(processor, Severity.DEBUG, "OPTS: " + contentNode.getDocument().getOptions());

		final String backend = (String) contentNode.getDocument().getOptions().getOrDefault("backend", "html5");
		//log(processor, Severity.DEBUG, "BACKEND: " + backend);

		final String baseDir = (String) contentNode.getDocument().getOptions().get("base_dir");
		final Path baseDirPath = Paths.get(baseDir).normalize();

		final String destinationDir = (String) contentNode.getDocument().getOptions().get("destination_dir");
		final Path destinationDirPath = Paths.get(destinationDir).normalize();

		final String imagesDir = (String) contentNode.getDocument().getAttributes().getOrDefault("imagesdir", "images");
		final Path imagesDirPath;
		if (backend.equalsIgnoreCase("pdf")) {
			imagesDirPath = baseDirPath.resolve(imagesDir).normalize();
		} else {
			imagesDirPath = destinationDirPath.resolve(imagesDir).normalize();
		}

		//log(processor, Severity.DEBUG, "IMAGESDIR PATH: " + imagesDirPath);

		return imagesDirPath;
	}

	private static void log(final Processor processor, final Severity severity, final String message) {
		if (processor != null) {
			try {
				processor.log(new LogRecord(severity, message));
			} catch (Exception ex) {
				//
			}
		}
	}

}
