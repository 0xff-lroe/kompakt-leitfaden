package at.funkfeuer.dokumentation.extensions;

import at.funkfeuer.dokumentation.extensions.browserframe.BrowserFrameBlockMacroProcessor;
import at.funkfeuer.dokumentation.extensions.browserframe.BrowserFrameInlineMacroProcessor;
import org.asciidoctor.Asciidoctor;
import org.asciidoctor.extension.JavaExtensionRegistry;
import org.asciidoctor.jruby.extension.spi.ExtensionRegistry;

public class ExtRegistry implements ExtensionRegistry {

    @Override
    public void register(final Asciidoctor asciidoctor) {
        final JavaExtensionRegistry javaExtensionRegistry = asciidoctor.javaExtensionRegistry();
        javaExtensionRegistry.treeprocessor(TableOfXTreeProcessor.class);
        javaExtensionRegistry.inlineMacro(BrowserFrameInlineMacroProcessor.class);
        javaExtensionRegistry.blockMacro(BrowserFrameBlockMacroProcessor.class);
    }
}
