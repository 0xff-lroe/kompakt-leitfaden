package at.funkfeuer.dokumentation.extensions;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.converter.JavaConverterRegistry;
import org.asciidoctor.jruby.converter.spi.ConverterRegistry;

public class ConvRegistry implements  ConverterRegistry {

    @Override
    public void register(final Asciidoctor asciidoctor) {
        final JavaConverterRegistry javaConverterRegistry = asciidoctor.javaConverterRegistry();
        javaConverterRegistry.register(LinkCheckConverter.class);
    }
}
