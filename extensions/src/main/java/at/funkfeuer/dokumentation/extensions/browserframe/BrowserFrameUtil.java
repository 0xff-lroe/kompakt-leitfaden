package at.funkfeuer.dokumentation.extensions.browserframe;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class BrowserFrameUtil {

	private BrowserFrameUtil() {
		//
	}

	public static BufferedImage browserFrame(final BufferedImage img, final BrowserFrame browserFrame) throws InterruptedException, IOException {
		return browserFrame(img, browserFrame, null);
	}

	public static BufferedImage browserFrame(final BufferedImage img, final BrowserFrame browserFrame, final String sUrl) throws InterruptedException, IOException {
		final int browserFrameBorderWidthLeft = browserFrame.getContentOffset().x;
		final int browserFrameBorderWidthRight = browserFrame.getFrameImage().getWidth() - browserFrame.getContentOffset().x - browserFrame.getContentDimension().width;
		final int browserFrameBorderHeightTop = browserFrame.getContentOffset().y;
		final int browserFrameBorderHeightBottom = browserFrame.getFrameImage().getHeight() - browserFrame.getContentOffset().y - browserFrame.getContentDimension().height;
		final int browserFrameBorderWidth = browserFrameBorderWidthLeft + browserFrameBorderWidthRight;
		final int browserFrameBorderHeight = browserFrameBorderHeightTop + browserFrameBorderHeightBottom;

		final int browserFrameContentCenterX = browserFrameBorderWidthLeft + browserFrame.getContentDimension().width / 2;
		final int browserFrameContentCenterY = browserFrameBorderHeightTop + browserFrame.getContentDimension().height / 2;

		final BufferedImage browserFrameImage = browserFrame.getFrameImage();
		/* /
		final Graphics2D browserFrameGraphics = browserFrameImage.createGraphics();
		final int browserFrameContentWidth = browserFrame.getContentDimension().width;
		final int browserFrameContentHeight = browserFrame.getContentDimension().height;

		browserFrameGraphics.setColor(Color.BLUE);
		browserFrameGraphics.setStroke(new BasicStroke(1));
		browserFrameGraphics.drawLine(browserFrameContentCenterX - 1, 0, browserFrameContentCenterX - 1, browserFrameImage.getHeight());
		browserFrameGraphics.drawLine(browserFrameContentCenterX + 1, 0, browserFrameContentCenterX + 1, browserFrameImage.getHeight());
		browserFrameGraphics.drawLine(0, browserFrameContentCenterY - 1, browserFrameImage.getWidth(), browserFrameContentCenterY - 1);
		browserFrameGraphics.drawLine(0, browserFrameContentCenterY + 1, browserFrameImage.getWidth(), browserFrameContentCenterY + 1);
		/* */

		// Draw border pieces
		final BufferedImage browserFrameTopLeftImage = browserFrameImage.getSubimage(0, 0, browserFrameContentCenterX - 1, browserFrameContentCenterY - 1);
		final BufferedImage browserFrameCenterLeftImage = browserFrameImage.getSubimage(0, browserFrameContentCenterY, browserFrameContentCenterX - 1, 1);
		final BufferedImage browserFrameBottomLeftImage = browserFrameImage.getSubimage(0, browserFrameContentCenterY + 1, browserFrameContentCenterX - 1, browserFrameImage.getHeight() - browserFrameContentCenterY - 1);

		final BufferedImage browserFrameTopRightImage = browserFrameImage.getSubimage(browserFrameContentCenterX + 1, 0, browserFrameContentCenterX - 1, browserFrameContentCenterY - 1);
		final BufferedImage browserFrameCenterRightImage = browserFrameImage.getSubimage(browserFrameContentCenterX + 1, browserFrameContentCenterY, browserFrameContentCenterX - 1, 1);
		final BufferedImage browserFrameBottomRightImage = browserFrameImage.getSubimage(browserFrameContentCenterX + 1, browserFrameContentCenterY + 1, browserFrameContentCenterX - 1, browserFrameImage.getHeight() - browserFrameContentCenterY - 1);

		final BufferedImage browserFrameTopCenterImage = browserFrameImage.getSubimage(browserFrameContentCenterX, 0, 1, browserFrameContentCenterY - 1);
		final BufferedImage browserFrameBottomCenterImage = browserFrameImage.getSubimage(browserFrameContentCenterX, browserFrameContentCenterY, 1,  browserFrameImage.getHeight() - browserFrameContentCenterY - 1);

		final Dimension resultDimension = new Dimension(img.getWidth() + browserFrameBorderWidth,img.getHeight() + browserFrameBorderHeight);
		final BufferedImage resultImage = new BufferedImage(resultDimension.width, resultDimension.height, BufferedImage.TYPE_INT_ARGB);
		final Graphics2D resultGraphics = resultImage.createGraphics();

		resultGraphics.drawImage(browserFrameTopLeftImage, 0,0, browserFrameTopLeftImage.getWidth(), browserFrameTopLeftImage.getHeight(), null);
		resultGraphics.drawImage(browserFrameCenterLeftImage, 0, browserFrameTopLeftImage.getHeight(), browserFrameTopLeftImage.getWidth(), resultDimension.height - browserFrameTopLeftImage.getHeight() - browserFrameBottomLeftImage.getHeight(), null);
		resultGraphics.drawImage(browserFrameBottomLeftImage, 0, resultImage.getHeight() - browserFrameBottomLeftImage.getHeight(),browserFrameBottomLeftImage.getWidth(), browserFrameBottomLeftImage.getHeight(), null);

		resultGraphics.drawImage(browserFrameTopRightImage, resultDimension.width - browserFrameTopLeftImage.getWidth(),0, browserFrameTopRightImage.getWidth(), browserFrameTopRightImage.getHeight(), null);
		resultGraphics.drawImage(browserFrameCenterRightImage, resultDimension.width - browserFrameTopLeftImage.getWidth(), browserFrameTopRightImage.getHeight(), browserFrameTopRightImage.getWidth(), resultDimension.height - browserFrameTopRightImage.getHeight() - browserFrameBottomRightImage.getHeight(), null);
		resultGraphics.drawImage(browserFrameBottomRightImage, resultDimension.width - browserFrameTopLeftImage.getWidth(), resultImage.getHeight() - browserFrameBottomRightImage.getHeight(),browserFrameBottomRightImage.getWidth(), browserFrameBottomRightImage.getHeight(), null);

		resultGraphics.drawImage(browserFrameTopCenterImage, browserFrameTopLeftImage.getWidth(), 0, resultDimension.width - browserFrameTopLeftImage.getWidth() - browserFrameTopRightImage.getWidth(), browserFrameTopCenterImage.getHeight(), null);
		resultGraphics.drawImage(browserFrameBottomCenterImage, browserFrameTopLeftImage.getWidth(), resultImage.getHeight() - browserFrameBottomCenterImage.getHeight() - 1, resultDimension.width - browserFrameTopLeftImage.getWidth() - browserFrameTopRightImage.getWidth(), browserFrameBottomCenterImage.getHeight(), null);

		// Draw text
		if (StringUtils.isNotBlank(sUrl)) {
			resultGraphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
			resultGraphics.setColor(Color.BLACK);
			resultGraphics.setFont(new Font("SansSerif", Font.PLAIN, browserFrame.getUrlFontSize()));
			resultGraphics.drawString(StringEscapeUtils.unescapeHtml4(sUrl.trim()), browserFrame.getUrlOffset().x, browserFrame.getUrlOffset().y);
		}

		// Draw image asynchronous - wait until the drawing is completed
		CountDownLatch latch = new CountDownLatch(1);
		boolean imageComplete = resultGraphics.drawImage(img, browserFrame.getContentOffset().x, browserFrame.getContentOffset().y, new ImageCompleteWaiter(latch));
		if (!imageComplete) {
			latch.await(10, TimeUnit.SECONDS);
		}
		return resultImage;
	}

	public static BufferedImage loadImage(final String path) throws IOException {
		if (StringUtils.isBlank(path)) {
			return null;
		}
		return ImageIO.read(BrowserFrameUtil.class.getClassLoader().getResourceAsStream(path));
	}

	public static BufferedImage loadImage(final File file) throws IOException {
		if (file == null || !file.exists()) {
			return null;
		}
		return ImageIO.read(file);
	}

	public static BufferedImage loadImage(final Path path) throws IOException {
		if (path == null || path.toFile() == null || !path.toFile().exists()) {
			return null;
		}
		return ImageIO.read(path.toFile());
	}

	public static boolean saveImagePNG(final BufferedImage image, final File file) throws IOException {
		if (image == null || file == null) {
			return false;
		}
		return ImageIO.write(image, "png", file);
	}

	public static boolean saveImagePNG(final BufferedImage image, final Path path) throws IOException {
		if (image == null || path == null || path.toFile() == null) {
			return false;
		}
		return ImageIO.write(image, "png", path.toFile());
	}

	public static Path getDerivedPath(final Path path, final String infix) {
		if (path == null ||StringUtils.isBlank(infix)) {
			return path;
		}

		final String fileName = path.getFileName().toString();
		final int extIndex = fileName.lastIndexOf(".");
		final String baseName = fileName.substring(0, extIndex);
		final String extension = fileName.substring(extIndex);

		return path.resolveSibling(baseName + infix + extension);
	}

	public static Path getDerivedPath(final Path path, final String infix, final String extension) {
		if (StringUtils.isBlank(extension)) {
			return getDerivedPath(path, infix);
		} else if (path == null || StringUtils.isBlank(infix)) {
			return path;
		}

		final String fileName = path.getFileName().toString();
		final int extIndex = fileName.lastIndexOf(".");
		final String baseName = fileName.substring(0, extIndex);
		final String tempExtension = extension.startsWith(".") ? extension : "." + extension;

		return path.resolveSibling(baseName + infix + tempExtension);
	}

}
