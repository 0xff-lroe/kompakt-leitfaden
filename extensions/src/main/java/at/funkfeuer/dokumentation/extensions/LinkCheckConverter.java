package at.funkfeuer.dokumentation.extensions;

import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.html.parser.HTMLParserListener;
import com.gargoylesoftware.htmlunit.javascript.SilentJavaScriptErrorListener;
import org.apache.commons.lang3.StringUtils;
import org.apache.hc.client5.http.auth.CredentialsProvider;
import org.apache.hc.client5.http.auth.StandardAuthScheme;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.config.RequestConfig;
import org.apache.hc.client5.http.cookie.BasicCookieStore;
import org.apache.hc.client5.http.cookie.CookieStore;
import org.apache.hc.client5.http.cookie.StandardCookieSpec;
import org.apache.hc.client5.http.impl.auth.BasicCredentialsProvider;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.*;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.asciidoctor.ast.*;
import org.asciidoctor.converter.ConverterFor;
import org.asciidoctor.converter.StringConverter;
import org.asciidoctor.log.LogRecord;
import org.asciidoctor.log.Severity;

import java.net.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@ConverterFor("link-check")
public class LinkCheckConverter extends StringConverter {

    private static final String[] SUPPORTED_URL_SCHEMES = new String[] {
            "http",
            "https"
    };

    private static final String NODE_TYPE_LINK = "link";

    private static final String ATTRIBUTE_TARGET = "target";
    private static final String ATTRIBUTE_TEXT = "text";
    private static final String ATTRIBUTE_ID = "id";
    private static final String ATTRIBUTE_ROLE = "role";
    private static final String ATTRIBUTE_TITLE = "title";

    private static final String OPT_CHECK_PRIVATE_IPV4 = "check-private-ipv4";

    private Map<String, Integer> urlCache = new HashMap<>();

    private final CookieStore httpCookieStore = new BasicCookieStore();
    private final CredentialsProvider httpCredentialsProvider = new BasicCredentialsProvider();
    // Create global request configuration
    private final RequestConfig httpDefaultRequestConfig = RequestConfig.custom()
            .setCookieSpec(StandardCookieSpec.RELAXED)
            .setExpectContinueEnabled(true)
            .setTargetPreferredAuthSchemes(Arrays.asList(StandardAuthScheme.NTLM, StandardAuthScheme.DIGEST))
            .setProxyPreferredAuthSchemes(Collections.singletonList(StandardAuthScheme.BASIC))
            .build();

    private final WebClient httpClient = getWebClientHTMLUnit();

    public LinkCheckConverter(final String backend, final Map<String, Object> opts) {
        super(backend, opts);
    }

    @Override
    public String getOutfileSuffix() {
        return "-link-check.csv";
    }

    @Override
    public String convert(final ContentNode node, final String transform, final Map<Object, Object> opts) {
        if (node != null && node.getNodeName() != null) {
            //log(Severity.INFO, "Node: " + node.getNodeName() + " [" + node.getId() + " / " + node.getContext() + " / " + transform + "]: " + node);

            if (node instanceof Document) {
                // Trigger further processing
                ((Document) node).getContent();

            } else if (node instanceof Section) {
                // Trigger further processing
                ((Section) node).getContent();

            } else if (node instanceof Block) {
                // Trigger further processing
                ((Block) node).getContent();

            } else if (node instanceof DescriptionList) {
                // Trigger further processing
                ((DescriptionList) node).getContent();

            } else if (node instanceof List) {
                // Trigger further processing
                final List list = (List) node;
                if (list.hasItems()) {
                    for (StructuralNode item : list.getItems()) {
                        if (item instanceof ListItem) {
                            ((ListItem) item).getText();
                        } else {
                            item.getContent();
                        }
                    }
                }

            } else if (node instanceof ListItem) {
                // Trigger further processing
                ((ListItem) node).getContent();

            } else if (node instanceof Table) {
                // Trigger further processing
                final Table table = (Table) node;
                if (table.getBody() != null) {
                    for (Row row : table.getBody()) {
                        for (Cell cell : row.getCells()) {
                            cell.getContent();
                        }
                    }
                }

            } else if (node instanceof Cell) {
                // Trigger further processing
                ((Cell) node).getContent();

            } else if (node instanceof PhraseNode) {
                final String sTransform;
                if (transform == null) {
                    sTransform = node.getNodeName();
                } else {
                    sTransform = transform;
                }

                if ("inline_anchor".equals(sTransform)) {
                    return findLinks((PhraseNode) node);
                }
            }
        }
        return "";
    }

    private String findLinks(final PhraseNode node) {
        final String type = node.getType();
        if (NODE_TYPE_LINK.equals(type)) {
            final String target = node.getTarget();

            if (StringUtils.startsWithAny(target, SUPPORTED_URL_SCHEMES)) {
                /*
                final Map<String, String> attrs = new HashMap<>();
                attrs.put(ATTRIBUTE_TARGET, target);

                final String text = node.getText();
                if (!target.equals(text)) {
                    attrs.put(ATTRIBUTE_TEXT, text);
                }

                if (StringUtils.isNotBlank(node.getId())) {
                    attrs.put(ATTRIBUTE_ID, node.getId());
                }

                final String role = node.getRole();
                if (StringUtils.isNotBlank(role) && !role.equals("bare")) {
                    attrs.put(ATTRIBUTE_ROLE, role);
                }

                final String title = node.getAttribute("title", "").toString();
                if (StringUtils.isNotBlank(title)) {
                    attrs.put(ATTRIBUTE_TITLE, title);
                }
                */

                //log(Severity.DEBUG, "LINK: " + ConsoleColors.YELLOW + target + ConsoleColors.RESET);

                final int responseCode;
                if (urlCache.containsKey(target)) {
                    responseCode = urlCache.get(target);
                } else {
                    int preliminaryResponseCode = getResponseCode(target);
                    if (preliminaryResponseCode < HttpStatus.SC_SUCCESS || preliminaryResponseCode >= HttpStatus.SC_REDIRECTION) {
                        // Try costlier method to get the response code using HTMLUnit
                        //log(Severity.DEBUG, "Check with HTMLUnit: " + ConsoleColors.YELLOW + target + ConsoleColors.RESET);
                        preliminaryResponseCode = getResponseCodeHTMLUnit(target);
                    }

                    responseCode = preliminaryResponseCode;
                    urlCache.put(target, responseCode);
                }

                if (responseCode >= HttpStatus.SC_SUCCESS && responseCode < HttpStatus.SC_REDIRECTION) {
                    log(Severity.INFO, "[" + ConsoleColors.GREEN + "✓" + ConsoleColors.RESET + "] Link check OK: " + target);
                } else {
                    log(Severity.WARN, "[" + ConsoleColors.RED + "✕" + ConsoleColors.RESET + "] Link check FAILED: " + target);
                }

                /*
                log(Severity.DEBUG, "");
                log(Severity.DEBUG, "");
                log(Severity.DEBUG, "");
                log(Severity.DEBUG, "");
                log(Severity.DEBUG, "");
                log(Severity.DEBUG, "");
                log(Severity.DEBUG, "");
                log(Severity.DEBUG, "");
                */

                return target + ";" + responseCode;
            }
        } else {
            //log(Severity.DEBUG, "Unknown anchor type: " + node.getType());
        }
        return "";
    }

    private WebClient getWebClientHTMLUnit() {
        java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(java.util.logging.Level.OFF);

        final WebClient httpclient = new WebClient();

        httpclient.getOptions().setThrowExceptionOnScriptError(false);
        httpclient.getOptions().setThrowExceptionOnFailingStatusCode(false);
        httpclient.getOptions().setCssEnabled(false);
        httpclient.getOptions().setJavaScriptEnabled(true);
        httpclient.getOptions().setDownloadImages(true);
        httpclient.getOptions().setRedirectEnabled(true);
        httpclient.getOptions().setWebSocketEnabled(false);

        httpclient.setJavaScriptTimeout(15000);
        //httpclient.waitForBackgroundJavaScript(5000);
        //httpclient.waitForBackgroundJavaScriptStartingBefore(5000);

        //httpclient.setAjaxController(new NicelyResynchronizingAjaxController());

        httpclient.setCssErrorHandler(new SilentCssErrorHandler());
        httpclient.setJavaScriptErrorListener(new SilentJavaScriptErrorListener());

        httpclient.setIncorrectnessListener((message, origin) -> {});

        httpclient.setHTMLParserListener(new HTMLParserListener() {
            @Override
            public void error(String message, URL url, String html, int line, int column, String key) {
                //
            }

            @Override
            public void warning(String message, URL url, String html, int line, int column, String key) {
                //
            }
        });

        return httpclient;
    }

    private int getResponseCode(final String url) {
        if (StringUtils.isBlank(url)) {
            return -1;
        }

        // Use this User-Agent as some sites block the native USer-Agent used by the library
        final String userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:93.0) Gecko/20100101 Firefox/93.0";

        try {
            try (final CloseableHttpClient httpclient = HttpClients.custom()
                    .setDefaultCookieStore(httpCookieStore)
                    .setDefaultCredentialsProvider(httpCredentialsProvider)
                    .setDefaultRequestConfig(httpDefaultRequestConfig)
                    .build()
                ) {
                final HttpGet httpGet = new HttpGet(url);
                httpGet.setHeader(HttpHeaders.USER_AGENT, userAgent);
                httpGet.setHeader(HttpHeaders.ACCEPT_LANGUAGE, "en-US,en;q=0.8");
                httpGet.setHeader(HttpHeaders.ACCEPT, "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
                httpGet.setHeader(HttpHeaders.CACHE_CONTROL, "no-cache");

                // Sort out IP-Addresses from private subnets - always return code "200"
                try {
                    final InetAddress ipv4Address = InetAddress.getByName(httpGet.getAuthority().getHostName());
                    if (ipv4Address.isLoopbackAddress() || ipv4Address.isSiteLocalAddress()) {
                        return HttpStatus.SC_SUCCESS;
                    }
                } catch (UnknownHostException ex) {
                    //
                }

                try (final CloseableHttpResponse httpResponse = httpclient.execute(httpGet)) {
                    final String response = EntityUtils.toString(httpResponse.getEntity());
                    //log(Severity.DEBUG, "Entity = " + response);
                    return httpResponse.getCode();
                }
            }
        } catch (Exception ex) {
            log(Severity.DEBUG, "Error checking link '" + url + "': " + ex);
        }
        return -1;
    }

    private int getResponseCodeHTMLUnit(final String url) {
        if (StringUtils.isBlank(url)) {
            return -1;
        }

        try {
            final URI httpUri = new URI(url);

            // Sort out IP-Addresses from private subnets - always return code "200"
            try {
                final InetAddress ipv4Address = InetAddress.getByName(httpUri.getHost());
                if (ipv4Address.isLoopbackAddress() || ipv4Address.isSiteLocalAddress()) {
                    return HttpStatus.SC_SUCCESS;
                }
            } catch (UnknownHostException ex) {
                //
            }

            return httpClient.getPage(url).getWebResponse().getStatusCode();
        } catch (Exception ex) {
            log(Severity.DEBUG, "Error checking link '" + url + "': " + ex);
        }
        return -1;
    }

    private void log(final Severity severity, final String message) {
        try {
            log(new LogRecord(severity, message));
        } catch (Exception ex) {
            //
        }
    }

}
