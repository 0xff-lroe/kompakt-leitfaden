package at.funkfeuer.dokumentation.extensions;

import org.apache.commons.lang3.StringUtils;
import org.asciidoctor.ast.Block;
import org.asciidoctor.ast.Document;
import org.asciidoctor.ast.Section;
import org.asciidoctor.ast.StructuralNode;
import org.asciidoctor.extension.Treeprocessor;
import org.asciidoctor.log.LogRecord;
import org.asciidoctor.log.Severity;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class TableOfXTreeProcessor extends Treeprocessor {

    protected final HashMap<Integer, String> figureMap = new HashMap<>();
    protected final HashMap<Integer, String> tableMap = new HashMap<>();

    private int figureCounter = 1;
    private int tableCounter = 1;

    @Override
    public Document process(final Document document) {
        processBlock(document);
        return document;
    }

    private void processBlock(final StructuralNode block) {
        try {
            // Process the whole document to find
            for (final StructuralNode currentBlock : block.getBlocks()) {
                try {
                    if (currentBlock != null) {
                        //log(Severity.DEBUG, "BLOCK Class: " + currentBlock.getClass().getName());
                        //log(Severity.DEBUG, "BLOCK Title: " + currentBlock.getTitle());
                        //log(Severity.DEBUG, "BLOCK Caption: " + currentBlock.getCaption());

                        final boolean bRecursive;
                        if (currentBlock.getContext() != null) {
                            if ("image".equalsIgnoreCase(currentBlock.getContext())) {
                                try {
                                    final String caption = currentBlock.getCaption();
                                    final String title = currentBlock.getTitle();

                                    if (StringUtils.isNotBlank(caption) || StringUtils.isNotBlank(title)) {
                                        currentBlock.setId(Constants.TOF_ANCHOR_PREFIX + formatCounterValue(figureCounter));
                                        if (StringUtils.isNotBlank(caption) && StringUtils.isNotBlank(title)) {
                                            figureMap.put(figureCounter, StringUtils.trim(caption + StringUtils.trim(title)));
                                        } else if (StringUtils.isNotBlank(caption) && StringUtils.isBlank(title)) {
                                            figureMap.put(figureCounter, StringUtils.trim(caption));
                                        } else {
                                            figureMap.put(figureCounter, StringUtils.trim(title));
                                        }
                                        figureCounter++;
                                    } else {
                                        if (currentBlock.getSourceLocation() != null) {
                                            log(Severity.INFO, "Figure without caption found (" + currentBlock.getSourceLocation().toString() + ")");
                                        } else {
                                            log(Severity.INFO, "Figure without caption found (Set plugin configuration property \"<sourcemap>true</sourcemap>\" to get the exact location)");
                                        }
                                    }
                                } catch (Exception ex) {
                                    if (currentBlock.getSourceLocation() != null) {
                                        log(Severity.DEBUG, "Error while parsing document (Context = image): " + ex.getMessage() + " (" + currentBlock.getSourceLocation().toString() + ")");
                                    } else {
                                        log(Severity.DEBUG, "Error while parsing document (Context = image): " + ex.getMessage() + " (Set plugin configuration property \"<sourcemap>true</sourcemap>\" to get the exact location)");
                                    }
                                }

                                // It's a matching block, so don't recursively descend into the child node
                                bRecursive = false;
                            } else if ("table".equalsIgnoreCase(currentBlock.getContext())) {
                                try {
                                    final String caption = currentBlock.getCaption();
                                    final String title = currentBlock.getTitle();

                                    if (StringUtils.isNotBlank(caption) || StringUtils.isNotBlank(title)) {
                                        currentBlock.setId(Constants.TOT_ANCHOR_PREFIX + formatCounterValue(tableCounter));
                                        if (StringUtils.isNotBlank(caption) && StringUtils.isNotBlank(title)) {
                                            tableMap.put(tableCounter, StringUtils.trim(caption + StringUtils.trim(title)));
                                        } else if (StringUtils.isNotBlank(caption) && StringUtils.isBlank(title)) {
                                            tableMap.put(tableCounter, StringUtils.trim(caption));
                                        } else {
                                            tableMap.put(tableCounter, StringUtils.trim(title));
                                        }
                                        tableCounter++;
                                    } else {
                                        if (currentBlock.getSourceLocation() != null) {
                                            log(Severity.INFO, "Table without caption found (" + currentBlock.getSourceLocation().toString() + ")");
                                        } else {
                                            log(Severity.INFO, "Table without caption found (Set plugin configuration property \"<sourcemap>true</sourcemap>\" to get the exact location)");
                                        }
                                    }
                                } catch (Exception ex) {
                                    if (currentBlock.getSourceLocation() != null) {
                                        log(Severity.DEBUG, "Error while parsing document (Context = table): " + ex.getMessage() + " (" + currentBlock.getSourceLocation().toString() + ")");
                                    } else {
                                        log(Severity.DEBUG, "Error while parsing document (Context = table): " + ex.getMessage() + " (Set plugin configuration property \"<sourcemap>true</sourcemap>\" to get the exact location)");
                                    }
                                }

                                // It's a matching block, so don't recursively descend into the child node
                                bRecursive = false;
                            } else {
                                // It's not a matching block, so recursively descend into the child node
                                bRecursive = true;
                            }
                        } else {
                            // It's not a matching block, so recursively descend into the child node
                            bRecursive = false;
                        }

                        if (bRecursive) {
                            if ("glossary".equalsIgnoreCase(currentBlock.getStyle()) && "dlist".equalsIgnoreCase(currentBlock.getNodeName())) {
                                // Skip glossary nodes
                            } else {
                                // It's not a matching block, so recursively descend into the child node
                                processBlock(currentBlock);
                            }
                        }
                    }
                } catch (Exception ex) {
                    log(Severity.WARN, "Error while parsing document: " + ex.getMessage());
                }
            }

            // Search for annotated sections with "[tof]"
            final Iterator<StructuralNode> blockIteratorToF = block.getBlocks().iterator();
            while (blockIteratorToF.hasNext()) {
                final StructuralNode currentBlock = blockIteratorToF.next();
                if (currentBlock != null) {
                    if (currentBlock instanceof Section) {
                        final Section currentSection = (Section) currentBlock;
                        if ("tof".equals(currentSection.getSectionName())) {
                            //log(Severity.DEBUG, "Try to create table-of-figures");

                            // Check meta data
                            if (currentSection.getAttributes().containsKey("tof-processed")) {
                                //log(Severity.DEBUG, "Already has processed table-of-figures");
                                continue;
                            }

                            final List<String> generatedLines = generateTableOfFiguresContents();
                            if (!generatedLines.isEmpty()) {
                                //log(Severity.DEBUG, "Try to create table-of-figures: 02");
                                final Block newBlock = createBlock(currentSection, "paragraph", generatedLines, currentSection.getAttributes());
                                //log(Severity.DEBUG, "BLOCK Class: " + newBlock.getClass().getName());
                                //log(Severity.DEBUG, "BLOCK Title: " + newBlock.getTitle());
                                //log(Severity.DEBUG, "BLOCK Caption: " + newBlock.getCaption());

                                if (newBlock != null) {
                                    //log(Severity.DEBUG, "Try to create table-of-figures: 03");
                                    //currentSection.getBlocks().set(0, newBlock);
                                    //log(Severity.DEBUG, "BLOCKS Class: " + currentSection.getBlocks().getClass().getName());

                                    currentSection.getBlocks().add(0, newBlock);

                                    // Set meta data
                                    currentSection.getAttributes().put("tof-processed", "1");
                                } else {
                                    log(Severity.WARN, "Unable to create block for table of figures");
                                }
                            } else {
                                log(Severity.INFO, "No entries for table of figures found");
                            }
                            break;
                        }
                    }
                }
            }

            // Search for annotated sections with "[tot]"
            final Iterator<StructuralNode> blockIteratorToT = block.getBlocks().iterator();
            while (blockIteratorToT.hasNext()) {
                final StructuralNode currentBlock = blockIteratorToT.next();
                if (currentBlock instanceof Section) {
                    final Section currentSection = (Section) currentBlock;
                    if ("tot".equals(currentSection.getSectionName())) {
                        //log(Severity.DEBUG, "Try to create table-of-tables");

                        // Check meta data
                        if (currentSection.getAttributes().containsKey("tot-processed")) {
                            //log(Severity.DEBUG, "Already has processed table-of-tables");
                            continue;
                        }

                        final List<String> generatedLines = generateTableOfTablesContents();
                        if (!generatedLines.isEmpty()) {
                            //log(Severity.DEBUG, "Try to create table-of-tables: 02");
                            final Block newBlock = createBlock(currentSection, "paragraph", generatedLines, currentSection.getAttributes());
                            //log(Severity.DEBUG, "BLOCK Class: " + newBlock.getClass().getName());
                            //log(Severity.DEBUG, "BLOCK Title: " + newBlock.getTitle());
                            //log(Severity.DEBUG, "BLOCK Caption: " + newBlock.getCaption());

                            if (newBlock != null) {
                                //log(Severity.DEBUG, "Try to create table-of-tables: 03");
                                //currentSection.getBlocks().set(0, newBlock);
                                //log(Severity.DEBUG, "BLOCKS Class: " + currentSection.getBlocks().getClass().getName());
                                currentSection.getBlocks().add(0, newBlock);

                                // Set meta data
                                currentSection.getAttributes().put("tot-processed", "1");
                            } else {
                                log(Severity.WARN, "Unable to create block for table of tables");
                            }
                        } else {
                            log(Severity.INFO, "No entries for table of tables found");
                        }
                        break;
                    }
                }
            }
        } catch (Exception ex) {
            log(Severity.ERROR, "Error in '" + TableOfXTreeProcessor.class.getName() + "': " + ex.getMessage());
        }
    }

    private List<String> generateTableOfFiguresContents() {
        final List<String> lines = new LinkedList<>();
        for (int i = 1; i <= figureMap.size(); i++) {
            final String line = "<a anchor=\"" + Constants.TOF_ANCHOR_PREFIX + formatCounterValue(i) + "\">" + figureMap.get(i) + "</a>";
            lines.add(line + "<br />");
        }
        return lines;
    }

    private List<String> generateTableOfTablesContents() {
        final List<String> lines = new LinkedList<>();
        for (int i = 1; i <= tableMap.size(); i++) {
            final String line = "<a anchor=\"" + Constants.TOT_ANCHOR_PREFIX + formatCounterValue(i) + "\">" + tableMap.get(i) + "</a>";
            lines.add(line + "<br />");
        }
        return lines;
    }

    private String formatCounterValue(final int counter) {
        return String.format("%04d", counter);
    }

    private void log(final Severity severity, final String message) {
        try {
            log(new LogRecord(severity, message));
        } catch (Exception ex) {
            //
        }
    }

}






