package at.funkfeuer.dokumentation.extensions.browserframe;

import at.funkfeuer.dokumentation.extensions.Util;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.asciidoctor.ast.ContentNode;
import org.asciidoctor.extension.*;
import org.asciidoctor.log.LogRecord;
import org.asciidoctor.log.Severity;

import java.awt.image.BufferedImage;
import java.nio.file.Path;
import java.util.*;

@Name("browserframe")
@Format(FormatType.LONG)
@DefaultAttribute(key = "url", value = "")
public class BrowserFrameInlineMacroProcessor extends InlineMacroProcessor {

    public BrowserFrameInlineMacroProcessor() {
        this(null);
    }

    public BrowserFrameInlineMacroProcessor(final String macroName) {
        this(macroName, new HashMap<>());
    }

    public BrowserFrameInlineMacroProcessor(final String macroName, final Map<String, Object> config) {
        super(macroName, config);
    }

    @Override
    public Object process(final ContentNode parent, final String target, final Map<String, Object> attributes) {
        // Generate framed image
        try {

            final Path imagesDirPath = Util.getImagesDirPath(this, parent);
            final Path targetPath = imagesDirPath.resolve(target).normalize();

            if (!targetPath.toFile().exists()) {
                log(Severity.ERROR, "Unable to load image '" + targetPath + "'");
                return null;
            }

            final BufferedImage image = BrowserFrameUtil.loadImage(targetPath);
            final BufferedImage browserFrameImage = BrowserFrameUtil.browserFrame(image, BrowserFrame.GENERIC, Util.getStringAttributeValue(attributes, "url"));

            final Path derivedTargetPath = BrowserFrameUtil.getDerivedPath(targetPath, "_browser_frame", "png");
            final Path derivedRelativeTargetPath = imagesDirPath.relativize(derivedTargetPath);

            if (BrowserFrameUtil.saveImagePNG(browserFrameImage, derivedTargetPath)) {
                // Process asciidoc
                final String newTarget = derivedRelativeTargetPath.toString().replace("\\","/");

                final Map<String, Object> options = new HashMap<>();
                options.put("type", "image");
                options.put("target", newTarget);

                final Map<String, Object> attrs  = new HashMap<>();
                attrs.putAll(attributes);
                attrs.remove("url");
                attrs.remove("title");
                attrs.remove("caption");

                final String title = (String) attributes.get("title");
                final String sTitle;
                if (StringUtils.isNotBlank(title)) {
                    sTitle = title.trim();
                } else {
                    sTitle = "";
                }

                final String caption = (String) attributes.get("title");
                final String sCaption;
                if (attributes.containsKey("caption")) {
                    sCaption = caption.trim();
                } else if (StringUtils.isNotBlank(sTitle)) {
                    sCaption = parent.getDocument().getAttributes().get("figure-caption") + " " + parent.getDocument().getAndIncrementCounter("figure-number") + ". ";
                } else {
                    sCaption = "";
                }

                if (!attrs.containsKey("alt")) {
                    attrs.put("alt", "");
                }
                if (!attrs.containsKey("default-alt")) {
                    attrs.put("default-alt", attrs.get("alt"));
                }

                attrs.put("title", sTitle);
                attrs.put("caption", sCaption);

                return createPhraseNode(parent, "image", newTarget, attrs, options);
            } else {
                log(Severity.ERROR, "Unable to save image");
            }
        } catch (Exception ex) {
            log(Severity.ERROR, "Unable to merge image with browser frame. Message: " + ExceptionUtils.getStackTrace(ex));
        }
        return null;
    }

    private void log(final Severity severity, final String message) {
        try {
            log(new LogRecord(severity, message));
        } catch (Exception ex) {
            //
        }
    }
}






