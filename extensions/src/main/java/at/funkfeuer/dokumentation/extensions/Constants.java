package at.funkfeuer.dokumentation.extensions;

public class Constants {

	public static final String TOF_ANCHOR_PREFIX = "_tof";

	public static final String TOT_ANCHOR_PREFIX = "_tot";

	private Constants() {
		//
	}
	
}
