package at.funkfeuer.dokumentation.extensions.browserframe;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

enum BrowserFrame {

    GENERIC("browser.png", new Point(6, 71), new Dimension(800, 600), new Point(120, 51), 15);

    private final String imageName;

    private final Point contentOffset;
    private final Dimension contentDimension;

    private final Point urlOffset;
    private final int urlFontSize;

    BrowserFrame(final String imageName, final Point contentOffset, final Dimension contentDimension, final Point urlOffset, final int urlFontSize) {
        this.imageName = imageName;
        this.contentOffset = contentOffset;
        this.contentDimension = contentDimension;
        this.urlOffset = urlOffset;
        this.urlFontSize = urlFontSize;
    }

    public BufferedImage getFrameImage() throws IOException {
        return ImageIO.read(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("browserframe/" + imageName)));
    }

    public Point getContentOffset() {
        return contentOffset;
    }

    public Dimension getContentDimension() {
        return contentDimension;
    }

    public Point getUrlOffset() {
        return urlOffset;
    }

    public int getUrlFontSize() {
        return urlFontSize;
    }

}
