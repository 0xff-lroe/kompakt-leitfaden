package at.funkfeuer.dokumentation.extensions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Options;
import org.asciidoctor.SafeMode;
import org.asciidoctor.log.LogHandler;
import org.asciidoctor.log.LogRecord;
import org.asciidoctor.util.ClasspathHelper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import java.io.IOException;

@TestInstance(Lifecycle.PER_CLASS)
public class TestTableOfXTreeProcessor {

	private final Logger log = LogManager.getLogger(TestTableOfXTreeProcessor.class);

	private final ClasspathHelper classpath;

	private Asciidoctor asciidoctor;

	private final String ADOC_SIMPLE = "simple.adoc";
	private final String ADOC_TABLE_OF_TABLES = "table-of-tables.adoc";

	TestTableOfXTreeProcessor() {
		classpath = new ClasspathHelper();
		classpath.setClassloader(this.getClass().getClassLoader());
	}

	@BeforeEach
	public void setUp() {
		asciidoctor = Asciidoctor.Factory.create();
		asciidoctor.javaExtensionRegistry().treeprocessor(TableOfXTreeProcessor.class);
		asciidoctor.registerLogHandler(logRecord -> {
			final String logMessage = logRecord.getMessage() + " [" + logRecord.getSourceFileName() + "; " + logRecord.getSourceMethodName() + "]";
			switch (logRecord.getSeverity()) {
				case INFO:
					log.info(logMessage);
				case WARN:
					log.warn(logMessage);
				case DEBUG:
					log.debug(logMessage);
				case ERROR:
					log.error(logMessage);
				case FATAL:
					log.fatal(logMessage);
				case UNKNOWN:
					log.info(logMessage);
				default:
					log.info(logMessage);
			}
		});
	}

	@AfterEach
	public void tearDown() {
		asciidoctor.unregisterAllExtensions();
		asciidoctor.shutdown();
		asciidoctor = null;
	}

	@Test
	public void testSimple() {
		asciidoctor.convertFile(classpath.getResource(ADOC_SIMPLE),
				Options.builder()
						.backend("pdf")
						.headerFooter(true)
						.safe(SafeMode.UNSAFE)
				.build()
		);
	}

	@Test
	public void testTableOfTables() {
		asciidoctor.convertFile(classpath.getResource(ADOC_TABLE_OF_TABLES),
				Options.builder()
						.backend("pdf")
						.docType("book")
						.headerFooter(true)
						.safe(SafeMode.UNSAFE)
						.build()
		);
	}

}
