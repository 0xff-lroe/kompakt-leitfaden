@ECHO OFF

SET WD=%CD%
SET SD=%~dp0
SET PARAMS=%*

cd "%SD%"

call build -Phtml -Ppdf-online -Ppdf-print -Ppdf-howto %PARAMS%

cd "%WD%"
