@ECHO OFF

SET WD=%CD%
SET SD=%~dp0
SET PARAMS=%*

cd "%SD%"

call deploy -Ppdf-online -Ppdf-print -Ppdf-howto %PARAMS%

cd "%WD%"
